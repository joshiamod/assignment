﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AwsFileProviderApi;
using System.Net;
using System.Threading;
using System.IO;
using System.Windows.Threading;

namespace Filer.Service
{
    public class FilerManager
    {

        private FileBObj _evaluation = null;
        private AwsFileProvider _fileProvider;
        private CancellationToken canToken;

        public CancellationToken CanToken
        {
            get
            {
                return canToken;
            }

            set
            {
                canToken = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<FileBObj> GetSavedFiles(string path)
        {
            List<FileBObj> lstFiles = new List<FileBObj>();
            //DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
            DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(path));
            if (di != null)
            {
                FileInfo[] subFiles = di.GetFiles();
                foreach (var item in subFiles)
                {
                    FileBObj obj = new FileBObj();
                    obj.FilePath = item.FullName;
                    obj.Name  = item.Name;
                    obj.CreatedWhen = item.CreationTime;
                    obj.FileType = item.Extension;
                    obj.FileSize = item.Length;
                    lstFiles.Add(obj); 
                }
            }

            return lstFiles;

        }
        public IEnumerable<Uri> GetOnlineFiles()
        {
            return GetFilesAws();
        }

        public void DownloadFileWait(CancellationToken canTok)
        {
            List<string> urls = new List<string>();
            int countFiles = 0;
            try
            {
                //urls.AddRange(GetFilesAws().Select(a => a.AbsoluteUri.ToString()));
                urls.Add(GetFilesAws().Select(a => a.AbsoluteUri.ToString()).FirstOrDefault());
                var loops = Parallel.ForEach(urls,
                        new ParallelOptions { MaxDegreeOfParallelism = 3, CancellationToken = canToken },
                            (l) =>
                            {
                                AwsDownloadHelper.DownloadFileWebClient(l, this.canToken);
                                countFiles = Interlocked.Increment(ref countFiles);
                                canTok.ThrowIfCancellationRequested();
                            }
                        );
                
            }
            catch (AggregateException ex)
            {
                System.Diagnostics.Debug.WriteLine("This is mine " +  ex.Message);
              //  var ShowCancellationMessage = new Action(viewModel.ShowCancellationMessage);
                //              Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, ShowCancellationMessage);
                //Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Normal, ShowCancellationMessage);
            }
        }

        public void DownloadFile()
        {

        }

        private IEnumerable<Uri>  GetFilesAws()
        {
            //evaluation = _repository.GetImmEvaluation(clientGuid);
            _fileProvider = new AwsFileProvider();
            return _fileProvider.Get();
        }
        public void UpdateFiles(string path)
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(Path.GetDirectoryName(path));
                //FileInfo[] infos = d.GetFiles("*.fil_").to;
                List<FileInfo> infos = d.GetFiles().OrderByDescending(a => a.CreationTime).ToList();
                var countFiles = infos.Count;
                for (int i = 0; i < countFiles; i++)
                {
                    if (i <= 9)
                    {
                        if (infos[i].Extension == ".fil_")
                        {
                            var name = infos[i].FullName.Replace(".fil_", "");
                            if (File.Exists(name))
                            {
                                File.Delete(name);
                                countFiles--;
                            }
                            File.Move(infos[i].FullName, infos[i].FullName.Replace(".fil_", ""));
                        }

                    }
                    else
                    {
                        if (File.Exists(infos[i].FullName))
                        {
                            File.Delete(infos[i].FullName);
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public void CleanUpOldFiles(string path, List<string> files)
        {
            DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(path));
            if (di != null)
            {
                foreach (var item in files)
                {
                    if (System.IO.File.Exists(item))
                    {
                        try
                        {
                            System.IO.File.Delete(item);
                        }
                        catch (System.IO.IOException e)
                        {
                            //Console.WriteLine(e.Message);
                            //return;
                        }
                    }
                }
            }
        }
        public void CleanUpTempFiles(string path)
        {
            DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(path));
            if (di != null)
            {
                FileInfo[] subFiles = di.GetFiles();
                foreach (var item in subFiles)
                {
                    if (item.Extension == ".fil_")
                    {
                        item.Delete(); 
                    }
                }
            }
        }
    }
}
