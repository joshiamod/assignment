﻿using System;

namespace Filer.Service
{
    public class FileBObj
    {
        public string Name { get; set; }
        public string FilePath { get; set; }
        public DateTime CreatedWhen { get; set; }

        public string FileType { get; set; }
        public long FileSize { get; set; }
    }
}