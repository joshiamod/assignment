﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Filer.Service
{
    public static class AwsDownloadHelper
    {

        public static EventHandler ProgressUpdated;
        public static EventHandler CompletedData;

        public static void DownloadFileHTTP(string url)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            var name = url.Substring(url.LastIndexOf('/') + 1);
            using (var res = (HttpWebResponse)req.GetResponse())
            using (var resStream = res.GetResponseStream())
            using (var fs = new FileStream("C:\\" + name, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                // Save to file
                var buffer = new byte[8 * 1024]; // 8 KB buffer
                int len; // Read count
                while ((len = resStream.Read(buffer, 0, buffer.Length)) > 0)
                    fs.Write(buffer, 0, buffer.Length);
            }
        }

        public static FileBObj DownloadFileWebClient(string url, CancellationToken token)
        {

            FileBObj fileDetails = null;
            var name = url.Substring(url.LastIndexOf('/') + 1);
            var path = @"C:\Amod\Test\" + name + ".fil_";
            try
            {
            
                using (var fs = new WebClient())
                {
                    token.Register(fs.CancelAsync);
                    fs.DownloadFile(new Uri(url), path);
                    fileDetails = GetFileDetailsFromHeader(fs.ResponseHeaders, name);
                    token.ThrowIfCancellationRequested();

                }
            }
            catch (OperationCanceledException ex)
            {
                throw new Exception();
            }
            return fileDetails;
        }

        private static FileBObj GetFileDetailsFromHeader(WebHeaderCollection fileHeader, string name)
        {
            FileBObj fileDetails = new FileBObj();
            fileDetails.CreatedWhen = DateTime.Parse(fileHeader.Get("Date"));
            //fileDetails.FilePath = fileHeader.fi
            fileDetails.FileType = fileHeader.Get("Content-Type");
            fileDetails.Name = name;
            fileDetails.FileSize = long.Parse(fileHeader.Get("Content-Length"));

            return fileDetails;
        }
        private static void Fs_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            //Console.WriteLine(((WebClient)sender).ResponseHeaders.ToString()); 
            if( e.UserState != null)
                Console.WriteLine(e.UserState.ToString());
        }

        private static void Fs_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            if (e != null)
                Console.WriteLine(e.ProgressPercentage.ToString());
        }
    }
}
