﻿
using Filer.Windows.ViewModel;
using Filer.Windows.Views;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Filer.Windows.View.Services
{
    public static class ViewServices
    {
        private static ConfigWindow _configDialog;
        internal static void CloseConfigDialog()
        {
            _configDialog.Close();
        }

        internal static void ShowConfigDialog(Window mainWindow, ConfigViewModel viewModel)
        {
            _configDialog = new Views.ConfigWindow();
            _configDialog.Owner = mainWindow;
            _configDialog.DataContext = viewModel;
            _configDialog.ShowDialog();
        }

    }
}