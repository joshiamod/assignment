﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Filer.Windows.ViewModel
{
    public abstract class ViewModelBase : INotifyPropertyChanging, INotifyPropertyChanged
    {
        public event PropertyChangingEventHandler PropertyChanging;
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void RaisePropertyChangedEvent([CallerMemberName]string propertyName = null)
        {
            if (IgnorePropertyChangeEvents) return;

            if (PropertyChanged == null) return;

            var e = new PropertyChangedEventArgs(propertyName);
            PropertyChanged(this, e);

            //if (PropertyChanged != null)
            //{
            //    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            //}
        }

        public virtual void RaisePropertyChangingEvent([CallerMemberName] string propertyName = null)
        {
            if (IgnorePropertyChangeEvents) return;

            if (PropertyChanging == null) return;

            var e = new PropertyChangingEventArgs(propertyName);
            PropertyChanging(this, e);
        }

        public virtual bool IgnorePropertyChangeEvents { get; set; }

    }
}
