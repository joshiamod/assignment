﻿using Filer.Windows.Commands;
using Filer.Windows.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Filer.Windows.ViewModel
{
    public class ConfigViewModel : ViewModelBase
    {
        private int parallelStreams;
        private string downloadFolder;
        private int interval;

        public ICommand SaveData { get; set; }


        public ConfigViewModel()
        {
            this.Initialize();
        }

        public int ParallelStreams
        {
            get
            {
                return parallelStreams;
            }

            set
            {
                base.RaisePropertyChangingEvent();
                parallelStreams = value;
                base.RaisePropertyChangedEvent();
            }
        }

        public string DownloadFolder
        {
            get
            {
                return downloadFolder;
            }

            set
            {
                downloadFolder = value;
                base.RaisePropertyChangingEvent();
                downloadFolder = value;
                base.RaisePropertyChangedEvent();

            }
        }

        public int Interval
        {
            get
            {
                return interval;
            }

            set
            {
                base.RaisePropertyChangingEvent();
                interval = value;
                base.RaisePropertyChangedEvent();

            }
        }

        public void LoadValues()
        {
            //this.parallelStreams = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["DegreeOfParallelism"].ToString());
            //this.downloadFolder = System.Configuration.ConfigurationSettings.AppSettings["DownloadFolder"].ToString();
            //this.interval = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["Interval"].ToString());

            this.parallelStreams = Settings.Default.Parallel;
            this.downloadFolder = Settings.Default.DowloadFolder;
            this.interval = Settings.Default.Interval;

        }

        public void SaveValues()
        {
            //System.Configuration.ConfigurationSettings.AppSettings.Set("DegreeOfParallelism", ParallelStreams.ToString());
            //System.Configuration.ConfigurationSettings.AppSettings.Set("DownloadFolder", DownloadFolder);
            //System.Configuration.ConfigurationSettings.AppSettings.Set("Interval", Interval.ToString());

            Settings.Default.Parallel = this.parallelStreams;
            Settings.Default.DowloadFolder = this.downloadFolder;
            Settings.Default.Interval = this.interval;
            Settings.Default.Save();
        }

        private void Initialize()
        {

            this.SaveData = new SaveCommand(this);

        }
    }
}
