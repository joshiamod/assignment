﻿
using Filer.Service;
using Filer.Windows.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Filer.Windows.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        public event EventHandler WorkStarted;
        public event EventHandler WorkEnded;
        private bool isDone;

        private IErrorCommandCallback _errorCallBack;


        public MainWindowViewModel()
        {
            this.Initialize();
        }

        public ICommand DoDemoWork { get; set; }
        public ICommand CancelWork { get; set; }
        public ICommand RefreshList { get; set; }
        internal CancellationTokenSource TokenSource { get; set; }
        public bool IsCancelled { get; set; }
        public ConfigViewModel ConfigVM { get; set; }
        public FileCollectionViewModel FileCollectionVM { get; set; }
        public List<FileBObj> test { get; set; }

        private string _buttonText;
        public bool IsDone
        {
            get
            {
                return isDone;
            }

            set
            {
                isDone = value;
                RaisePropertyChangedEvent();
            }
        }

        public IErrorCommandCallback ErrorCallBack
        {
            get
            {
                return _errorCallBack;
            }

            set
            {
                _errorCallBack = value;
            }
        }

        public string ButtonText
        {
            get
            {
                return _buttonText;
            }

            set
            {
                _buttonText = value;
                RaisePropertyChangedEvent();
            }
        }

        internal void RaiseWorkStartedEvent()
        {
            if (WorkStarted == null) return;

            WorkStarted(this, new EventArgs());
        }

        internal void RaiseExceotiob(Exception ex)
        {
            throw ex;
        }

        internal void RaiseWorkEndedEvent()
        {
            if (WorkEnded == null) return;

            this.FileCollectionVM.IsDone = true;

            Application.Current.Dispatcher.BeginInvoke(
                     DispatcherPriority.Normal,
                     new Action(delegate ()
                    {
                        FileCollectionVM.LoadDownloadedFiles(true);
                    }));
            this.ButtonText = "Sync";
             WorkEnded(this, new EventArgs());
        }

        public void RegisterCallback(IErrorCommandCallback configCommandCallback)
        {
            ErrorCallBack = configCommandCallback;
        }

        private void Initialize()
        {

            this.FileCollectionVM = new ViewModel.FileCollectionViewModel();
            FileCollectionVM.Name = "Test";
            FileCollectionVM.FileCount = 10;
            FileCollectionVM.LastUpdated = DateTime.Now;
            //FileCollectionVM.LoadDownloadedFiles();
            this.RefreshList = new LoadFilesCommand(this);
            // Initialize command properties
            this.ConfigVM = new ViewModel.ConfigViewModel();
            this.ConfigVM.LoadValues();
            try
            {
                this.DoDemoWork = new DownloadFilesCommand(this);
            }
            catch (Exception ex)
            {
                ErrorCallBack.ShowMessage(ex.Message);
            }

            this.CancelWork = new CancelDownloadCommand(this);
            this.ButtonText = "Sync";
        }
    }
}