﻿using Filer.Service;
using Filer.Windows.DataObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Data;

namespace Filer.Windows.ViewModel
{
    public class FileCollectionViewModel : ViewModelBase
    {
        private string name;
        private int fileCount;
        private DateTime lastUpdated;
        private FilerManager _fm;

        private IErrorCommandCallback _errorCallBack;

        //TODO: chane to appropriate later
        private const string path = @"c:\Amod\test\";

        public FileCollectionViewModel()
        {
            _fm = new FilerManager();
            LoadDownloadedFiles(false);
            //BindingOperations.EnableCollectionSynchronization(_fileList, _fm);
        }

        private List<FileBObj> _savedFileList;
        private List<FileBObj> _downloadedFileList;
        private ObservableCollection<FileDetails> _fileList;
        private ObservableCollection<FileDetails> _downloadFileList;
        private bool _isDone;

        public ConfigViewModel ConfigVM { get; set; }
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChangedEvent();
                }
            }
        }

        public int FileCount
        {
            get
            {
                return fileCount;
            }

            set
            {
                if (fileCount != value)
                {
                    fileCount = value;
                    RaisePropertyChangedEvent();
                }
            }
        }

        public DateTime LastUpdated
        {
            get
            {
                return lastUpdated;
            }

            set
            {
                if (lastUpdated != value)
                {
                    lastUpdated = value;
                    RaisePropertyChangedEvent();
                }
            }
        }

        public ObservableCollection<FileDetails> DownloadFileList
        {
            get
            {
                if (_downloadFileList == null)
                {
                    _downloadFileList = new ObservableCollection<FileDetails>(this.GetObservableCollection(DownloadedFileList));
                }

                return _downloadFileList;
            }

        }
        public ObservableCollection<FileDetails> FileList
        {
            get
            {
                return _fileList;
            }
            set
            {
                _fileList = value;
                RaisePropertyChangedEvent(); 
            }
        }

        public List<FileBObj> SavedFileList
        {
            get
            {
                ConfigVM = new ViewModel.ConfigViewModel();
                ConfigVM.DownloadFolder = @"C:\Amod\Test\";
                _savedFileList = _fm.GetSavedFiles(ConfigVM.DownloadFolder).ToList();

                return _savedFileList;
            }
        }

        public List<FileBObj> DownloadedFileList
        {
            get
            {
                return _downloadedFileList;
            }

            set
            {
                _downloadedFileList = value;
            }
        }

        public bool IsDone
        {
            get
            {
                return _isDone;
            }

            set
            {
                _isDone = value;
                RaisePropertyChangedEvent();
            }
        }

        public void LoadDownloadedFiles(bool update)
        {
            try
            {
                if (!update)
                {
                    FileList = new ObservableCollection<FileDetails>(this.GetObservableCollection(SavedFileList));
                }
                else
                {
                    _fm.UpdateFiles(path);
                    FileList = new ObservableCollection<FileDetails>(this.GetObservableCollection(SavedFileList));
                }
            }
            catch (Exception ex)
            {
                _errorCallBack.ShowMessage(ex.Message);
            }
        }

        public void RegisterCallback(IErrorCommandCallback errorCallback)
        {
            _errorCallBack = errorCallback;
        }

        private List<FileDetails> GetObservableCollection(List<FileBObj> lstFile)
        {
            var fileList = new List<FileDetails>();
            foreach (var item in lstFile)
            {
                FileDetails df = new FileDetails() { CreatedDate = item.CreatedWhen, FileType = item.FileType, Name = item.Name, FileUri = item.FilePath,FileSize = item.FileSize };
                fileList.Add(df);
            }

            return fileList;
        }
    }
}
