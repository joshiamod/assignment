﻿using Filer.Windows.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Filer.Windows.Commands
{
    public class SaveCommand : ICommand
    {
        private ConfigViewModel m_ConfigViewModel;

        public event EventHandler CanExecuteChanged;


        public SaveCommand(ConfigViewModel _configVm)
        {
            m_ConfigViewModel = _configVm;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            this.m_ConfigViewModel.SaveValues();
            
        }
    }
}
