﻿using Filer.Windows.ViewModel;
using System;
using System.Windows.Input;

namespace Filer.Windows.Commands
{
    public class CancelDownloadCommand : ICommand
    {
        private MainWindowViewModel m_ViewModel;

        public CancelDownloadCommand(MainWindowViewModel viewModel)
        {
            m_ViewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            //return true;
            return m_ViewModel.IsDone;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {

            if (m_ViewModel.IsCancelled) return;

            if (m_ViewModel.TokenSource == null)
            {
                throw new ApplicationException("TokenSource property is null");
            }

            m_ViewModel.TokenSource.Cancel();
            m_ViewModel.IsDone = false;
            m_ViewModel.ButtonText = "Sync";
        }
    }
}