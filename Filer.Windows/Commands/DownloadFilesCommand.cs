﻿using Filer.Service;
using Filer.Windows.DataObjects;
using Filer.Windows.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Filer.Windows.Commands
{
    public class DownloadFilesCommand : ICommand
    {
        private MainWindowViewModel m_ViewModel;
        private Filer.Service.FilerManager m_FM;
        public DownloadFilesCommand(MainWindowViewModel viewModel)
        {
            m_ViewModel = viewModel;
            m_ViewModel.FileCollectionVM.DownloadedFileList = new List<FileBObj>();
        }

        public bool CanExecute(object parameter)
        {
            return !m_ViewModel.IsDone;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        private List<FileBObj> lst { get; set; }

        public void Execute(object parameter)
        {
            m_FM = new FilerManager();
            m_ViewModel.TokenSource = new CancellationTokenSource();
            m_ViewModel.ButtonText = "Sync In-Progress...";
            lst = new List<FileBObj>();
            m_ViewModel.RaiseWorkStartedEvent();
            List<string> urls = new List<string>();
            //var taskOne = Task.Factory.StartNew(() => m_FM.DownloadFileWait(CancellationToken.None), m_ViewModel.TokenSource.Token);
            urls.AddRange(m_FM.GetOnlineFiles().Select(a => a.AbsoluteUri.ToString()));
            Task taskOne = null;
            bool parallelThrew = false;
             taskOne = Task.Run((() =>
                {
                    try
                    {
                        Parallel.ForEach(urls,
                        new ParallelOptions { MaxDegreeOfParallelism = m_ViewModel.ConfigVM.ParallelStreams, CancellationToken = m_ViewModel.TokenSource.Token },
                            (l) =>
                            {
                                //m_ViewModel.FileCollectionVM.DownloadedFileList.Add(AwsDownloadHelper.DownloadFileWebClient(l, m_ViewModel.TokenSource.Token));
                                AwsDownloadHelper.DownloadFileWebClient(l, m_ViewModel.TokenSource.Token);
                                m_ViewModel.TokenSource.Token.ThrowIfCancellationRequested();
                                //throw new Exception();
                            }
                        );
                    }
                    catch (OperationCanceledException ex)
                    {
                        var ShowCancellationMessage = new Action(() => m_ViewModel.ErrorCallBack.ShowMessage("Hmmm. Please check your connection!"));
                        Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, ShowCancellationMessage);
                    }
                    catch (AggregateException aex)
                    {
                        if (m_ViewModel.TokenSource.IsCancellationRequested)
                        {
                            var ShowCancellationMessage = new Action(() => m_ViewModel.ErrorCallBack.ShowMessage("Hmmm. The request was cancelled!"));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, ShowCancellationMessage);
                        }
                    }
                }
                ), m_ViewModel.TokenSource.Token);

            m_ViewModel.IsDone = true;
            //var taskTwo = taskOne.ContinueWith(t => { if (taskOne.Exception != null) { m_ViewModel.RaiseExceotiob(new Exception()); }});
            taskOne.ContinueWith(t => m_ViewModel.RaiseWorkEndedEvent(),TaskContinuationOptions.AttachedToParent);
            
        }
    }
}