﻿using Filer.Windows.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Filer.Windows.Commands
{
    public class LoadFilesCommand : ICommand
    {
        private FileCollectionViewModel m_fileCollectionVM;
        public event EventHandler CanExecuteChanged;
        private bool isDone;

        public LoadFilesCommand(MainWindowViewModel _fileCollectionVM)
        {
            m_fileCollectionVM = _fileCollectionVM.FileCollectionVM;
            isDone = _fileCollectionVM.IsDone;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            m_fileCollectionVM.LoadDownloadedFiles(false);//.FileList.RemoveAt(0);
        }
    }
}
