﻿using Filer.Windows.View.Services;
using Filer.Windows.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Filer.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IErrorCommandCallback
    {

        private MainWindowViewModel _mainWindowViewModel;
        
        private System.Threading.CancellationTokenSource cantoken;
        public MainWindow()
        {
            InitializeComponent();

            _mainWindowViewModel = new ViewModel.MainWindowViewModel();
            this.DataContext = _mainWindowViewModel;
            _mainWindowViewModel.RegisterCallback(this);
            _mainWindowViewModel.FileCollectionVM.RegisterCallback(this);
            FilerView.DataContext = _mainWindowViewModel.FileCollectionVM;
            _mainWindowViewModel.WorkEnded += _mainWindowViewModel_WorkEnded;
                
        }

        private void _mainWindowViewModel_WorkEnded(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var viewModel = (MainWindowViewModel)e.NewValue;
            viewModel.WorkStarted += ViewModel_WorkStarted;
        }

        
        private void ViewModel_WorkStarted(object sender, EventArgs e)
        {
           // throw new NotImplementedException();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var configDialog = _mainWindowViewModel.ConfigVM;
            ViewServices.ShowConfigDialog(this, configDialog);
        }

        public void ShowError(Exception ex)
        {
           // ScmExceptionHandler.ShowErrorMessage(this, ex);
        }

        public MessageBoxResult ShowMessage(string message, string header = "Error Message", MessageBoxImage icon = MessageBoxImage.Error, MessageBoxButton button = MessageBoxButton.OK)
        {
            return MessageBox.Show(this, message, header, button, icon);
        }
    }
}
