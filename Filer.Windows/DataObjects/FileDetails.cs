﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filer.Windows.DataObjects
{
    public class FileDetails : IEqualityComparer
    {
        private string fileUri;
        private string name;
        private string fileType;
        private DateTime createdDate;
        private long fileSize;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                    name = value;
                
            }
        }

        public string FileType
        {
            get
            {
                return fileType;
            }

            set
            {
                    fileType = value;
                
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return createdDate;
            }

            set
            {
                    createdDate = value;
            }
        }

        public string FileUri
        {
            get
            {
                return fileUri;
            }

            set
            {
                fileUri = value;
            }
        }

        public long FileSize
        {
            get
            {
                return fileSize;
            }

            set
            {
                fileSize = value;
            }
        }

        //public override bool Equals(object obj)
        //{
        //    var obj1 = (FileDetails)obj;

        //    return (obj1.CreatedDate == this.CreatedDate && obj1.FileSize == this.FileSize && obj1.Name == this.Name);
            
        //}

        //public override int GetHashCode()
        //{
        //    return this.ToString().GetHashCode();
        //}

        public new bool Equals(object x, object y)
        {
            throw new NotImplementedException();
        }

        public int GetHashCode(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
