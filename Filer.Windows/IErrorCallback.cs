﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Filer.Windows
{
    public interface IErrorCommandCallback
    {
        void ShowError(Exception ex);
        MessageBoxResult ShowMessage(string message, string header = "Error Message", MessageBoxImage icon = MessageBoxImage.Error, MessageBoxButton button = MessageBoxButton.OK);
    }
}
